// Palindrom-Funktion
function istPalindrom(str) {
  const reversed = str.split("").reverse().join("");
  return str === reversed;
}


// Funktion zum Testen der Palindrom-Funktion
function testePalindrom() {
  const testfaelle = [
    { wort: "radar", erwartet: true }, // Testfall 1
    { wort: "hello", erwartet: false }, // Testfall 2
    { wort: "level", erwartet: true }, // Testfall 3
    { wort: "deified", erwartet: true }, // Testfall 4
    { wort: "algorithm", erwartet: false }, // Testfall 5
  ];

  testfaelle.forEach((testfall) => {
    console.log(
      istPalindrom(testfall.wort) === testfall.erwartet
        ? "Test succeeded"
        : "Test failed",
    );
  });
}

testePalindrom();

