// Fibonacci-Rekursionsfunktion
function berechneFibonacci(n) {
  if (n <= 1) return n;
  return berechneFibonacci(n - 1) + berechneFibonacci(n - 2);
}

// Leistungstests für die Fibonacci-Funktion
function fuehreLeistungstestsDurch() {
  const testData = [
    { in: 1, out: 1 },
    { in: 5, out: 5 },
    { in: 6, out: 8 },
    { in: 15, out: 610 },
    { in: 34, out: 5702887 },
  ];

  testData.forEach((test) => {
    const startTime = new Date().getTime();
    const result = berechneFibonacci(test.in);
    const endTime = new Date().getTime();
    const duration = endTime - startTime;

    if (result !== test.out) {
      console.log(
        `Test failed for input ${test.in}. Incorrect result: ${result}`,
      );
    } else {
      console.log(
        `Test succeeded for input ${test.in}. Duration: ${duration}ms`,
      );
    }

    if (duration > 1000) {
      console.log(
        `Warning: The calculation took more than 1 second for input ${test.in}. Consider optimizing the implementation.`,
      );
    }
  });
}

fuehreLeistungstestsDurch();
