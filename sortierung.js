// Sortierfunktion
function sortiereListe(liste) {
  return liste.slice().sort((a, b) => a - b);
}


// Funktion zum Testen der Sortierfunktion
function testeSortierfunktion() {
  const data = [
    { in: [3, 1, 4, 1, 5], out: [1, 1, 3, 4, 5] },
    { in: [9, 2, 6, 5, 8], out: [2, 5, 6, 8, 9] },
    { in: [2, 1, 3], out: [1, 2, 3] },
    { in: [0], out: [0] },
    { in: [2, 1], out: [1, 2] },
  ];

  data.forEach((x) => {
    const copy = x.in.slice();
    const sortedList = sortiereListe(x.in);

    if (
      JSON.stringify(copy) !== JSON.stringify(x.in) ||
      JSON.stringify(sortedList) !== JSON.stringify(x.out)
    ) {
      console.log("Test failed");
    } else {
      console.log("Test succeeded");
    }

    // Check for side effects
    if (JSON.stringify(x.in) !== JSON.stringify(copy)) {
      console.log("Side effect detected");
    }
  });
}

testeSortierfunktion();

