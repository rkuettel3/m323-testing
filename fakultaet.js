// Fakultätsfunktion
function berechneFakultaet(n) {
  if (n === 0 || n === 1) {
    return 1;
  } else {
    return n * berechneFakultaet(n - 1);
  }
}


// Funktion zum Testen der Fakultätsfunktion
function testeFakultaet() {
  const data = [
    { input: 0, output: 1 },
    { input: 1, output: 1 },
    { input: 4, output: 24 },
    { input: 10, output: 3628800 },
  ];

  data.forEach((x) => {
    console.log(
      berechneFakultaet(x.input) === x.output
        ? "Test succeeded"
        : "Test failed",
    );
  });
}

testeFakultaet();

